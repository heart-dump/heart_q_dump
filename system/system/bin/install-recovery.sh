#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:100663296:680016a1ee0e0abd2e71087df040ef13c785c587; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:82e1ed89078e202e9b3249ab389414c7ee4a6f53 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:100663296:680016a1ee0e0abd2e71087df040ef13c785c587 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
